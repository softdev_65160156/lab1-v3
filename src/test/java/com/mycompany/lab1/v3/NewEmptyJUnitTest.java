/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/UnitTests/JUnit5TestClass.java to edit this template
 */
package com.mycompany.lab1.v3;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author Windows10
 */
public class NewEmptyJUnitTest {
    
    public NewEmptyJUnitTest() {
    }

    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    // TDD test Driven Development
    @Test
    public void testCheckWinRow1_X_output_true(){
        String[][] table = {{"X","X","X"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow2_X_output_true(){
        String[][] table = {{"-","-","-"},{"X","X","X"},{"-","-","-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow3_X_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"X","X","X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow1_O_output_true(){
        String[][] table = {{"O","O","O"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow2_O_output_true(){
        String[][] table = {{"-","-","-"},{"O","O","O"},{"-","-","-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow3_O_output_true(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"O","O","O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinRow1_O_output_false(){
        String[][] table = {{"-","O","O"},{"-","-","-"},{"-","-","-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinRow2_O_output_false(){
        String[][] table = {{"-","-","-"},{"-","O","O"},{"-","-","-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinRow3_O_output_false(){
        String[][] table = {{"-","-","-"},{"-","-","-"},{"-","O","O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinCol1_X_output_true(){
        String[][] table = {{"X","-","-"},{"X","-","-"},{"X","-","-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinCol2_X_output_true(){
        String[][] table = {{"-","X","-"},{"-","X","-"},{"-","X","-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinCol3_X_output_true(){
        String[][] table = {{"-","-","X"},{"-","-","X"},{"-","-","X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinCol1_X_output_false(){
        String[][] table = {{"-","-","-"},{"X","-","-"},{"X","-","-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinCol2_X_output_false(){
        String[][] table = {{"-","X","-"},{"-","-","-"},{"-","X","-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinCol3_X_output_false(){
        String[][] table = {{"-","-","X"},{"-","-","X"},{"-","-","-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinXR_X_output_true(){
        String[][] table = {{"X","-","-"},{"-","X","-"},{"-","-","X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinXR_X_output_false(){
        String[][] table = {{"X","-","-"},{"-","O","-"},{"-","-","X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }

    public void testCheckWinXR_O_output_true(){
        String[][] table = {{"O","-","-"},{"-","O","-"},{"-","-","O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    public void testCheckWinXR_O_output_false(){
        String[][] table = {{"O","-","-"},{"-","X","-"},{"-","-","O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinXL_X_output_true(){
        String[][] table = {{"-","-","X"},{"-","X","-"},{"X","-","-"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinXL_X_output_false(){
        String[][] table = {{"X","-","-"},{"-","O","-"},{"-","-","X"}};
        String currentPlayer = "X";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckWinXL_O_output_true(){
        String[][] table = {{"-","-","O"},{"-","O","-"},{"O","-","-"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(true,result);
    }
    @Test
    public void testCheckWinXL_O_output_false(){
        String[][] table = {{"O","-","-"},{"-","X","-"},{"-","-","O"}};
        String currentPlayer = "O";
        boolean result = XO.checkWin(table, currentPlayer);
        assertEquals(false,result);
    }
    @Test
    public void testCheckDraw_0_output_false(){
        int turn = 0;
        boolean result = XO.checkDraw(turn);
        assertEquals(false,result);
    }
    @Test
    public void testCheckDraw_1_output_false(){
        int turn = 1;
        boolean result = XO.checkDraw(turn);
        assertEquals(false,result);
    }
    @Test
    public void testCheckDraw_2_output_false(){
        int turn = 2;
        boolean result = XO.checkDraw(turn);
        assertEquals(false,result);
    }
    @Test
    public void testCheckDraw_3_output_false(){
        int turn = 3;
        boolean result = XO.checkDraw(turn);
        assertEquals(false,result);
    }@Test
    public void testCheckDraw_4_output_false(){
        int turn = 4;
        boolean result = XO.checkDraw(turn);
        assertEquals(false,result);
    }
    
    @Test
    public void testCheckDraw_5_output_false(){
        int turn = 5;
        boolean result = XO.checkDraw(turn);
        assertEquals(false,result);
    }
    
    @Test
    public void testCheckDraw_6_output_false(){
        int turn = 6;
        boolean result = XO.checkDraw(turn);
        assertEquals(false,result);
    }
    @Test
    public void testCheckDraw_7_output_false(){
        int turn = 7;
        boolean result = XO.checkDraw(turn);
        assertEquals(false,result);
    }
    @Test
    public void testCheckDraw_8_output_false(){
        int turn = 8;
        boolean result = XO.checkDraw(turn);
        assertEquals(false,result);
    }
    
    @Test
    public void testCheckDraw_9_output_true(){
        int turn = 9;
        boolean result = XO.checkDraw(turn);
        assertEquals(true,result);
    }
    @Test
    public void testCheckDraw_10_output_true(){
        int turn = 10;
        boolean result = XO.checkDraw(turn);
        assertEquals(false,result);
    }
}
